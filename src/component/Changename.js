import React, { useContext, useState } from "react";
import { usercontext } from "./Home";

const Changename = () => {
  const val = useContext(usercontext);
  const [nam, setnam] = useState(val);

  return (
    <div>
      <input
        type="text"
        onChange={(e) => {
          setnam(e.target.value);
        }}
      />
      <h1>{`your name is${nam}`}</h1>
    </div>
  );
};

export default Changename;
