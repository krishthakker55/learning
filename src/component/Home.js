import React, { createContext, useState } from "react";
import Changename from "./Changename";
export const usercontext = createContext();
const Home = () => {
  const [val, setval] = useState("krish");
  return (
    <usercontext.Provider value={val}>
      <Changename />
    </usercontext.Provider>
  );
};

export default Home;
