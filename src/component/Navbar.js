import React from "react";
import { NavLink } from "react-router-dom";
import { Button } from "reactstrap";

const Navbar = () => {
  return (
    <>
      <div className="card shadow m-2">
        <nav className="navbar p-3">
          <ul className="nav">
            <li className="nav-item">
              <NavLink className="nav-link" to="/admin/home">
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/admin/about">
                About us
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/admin/contact">
                Contact us
              </NavLink>
            </li>
          </ul>
          <ul className="nav">
            <li className="nav-item ">
              <Button className="btn">Login</Button>
            </li>
            <li className="nav-item ">
              <Button className="btn btn-primary">Sign up</Button>
            </li>
          </ul>
        </nav>
      </div>
    </>
  );
};

export default Navbar;
