import React, { useState } from "react";
import { Navbar } from "reactstrap";

const About = () => {
  const [count, setcount] = useState(0);
  const fun = () => {
    setcount(count + 2);
  };
  return (
    <div>
      <Navbar />
      <h1>count={count}</h1>
      <button onClick={fun}>counter</button>
    </div>
  );
};

export default About;
