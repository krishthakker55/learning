import React from "react";
import { Outlet, Navigate } from "react-router-dom";
import Navbar from "../component/Navbar";

const Admin = () => {
  const abc = true;
  return <div>{abc ? <Outlet /> : <Navigate to="/error" />}</div>;
};

export default Admin;
