import React from "react";
import { Route, Routes } from "react-router-dom";
import Layout from "../component/Layout";
import Contact from "../component/Contact";
import Home from "../component/Home";
import About from "../component/About";
import Admin from "./Admin";
import Error from "../component/Error";
const Router = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/admin" element={<Admin />}>
            <Route path="home" element={<Home />} />
            <Route path="contact" element={<Contact />} />
            <Route path="about" element={<About />} />
          </Route>
        </Route>
        <Route path="error" element={<Error />} />
      </Routes>
    </div>
  );
};

export default Router;
